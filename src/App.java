import models.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();

        Circle circle2 = new Circle(3.0);

        System.out.println("Circle 1");
        System.out.println(circle1.getRadius());
        System.out.println(circle1.getArea());
        System.out.println(circle1.getCircumference());
        System.out.println(circle1.toString());

        System.out.println("Circle 2");
        System.out.println(circle2.getRadius());
        System.out.println(circle2.getArea());
        System.out.println(circle2.toString());
    }
}
